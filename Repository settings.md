# Repository setup

Below are the recommended settings for new repositories, created in this group

## TL;DR

Here is the short summary of what will be explained in more details below.

* Important branches should be protected (`main`, `develop`, etc.) and only maintainers should be allowed to push
* Avoid giving permanent access to repository, always specify short deadlines, so you don't have to revoke accesses manually later
* Set up CI/CD for your repository as soon as possible
* Shared runners should be disabled for projects with CI/CD
* We recommend making Production deployment step manual (not automatic)
* Ensure that your CI/CD variables are using proper "Protected" + "Masked" configuration
* Configure environments if you need to have multiple sets of CI/CD variables for different environments
 
## Security settings

### Branches

We recommend protecting all important branches (such as `main`, `develop`, etc.). Recommended set up is:
* Pushing allowed only for mainteiners
* Force pushing is disabled
* Merging is allowed for maintainers and optionally allowed for developers (if processes on the project allow that)

### Accesses

If new people are added/removed to the project often, we recommend to set expiration time for repository access. This will ensure that people who are released from project will lose their access with time.

## CI/CD settings

We recommend setting up CI/CD for all repositories. Check out other repos for samples (especially for .NET or front-end repos).

### Deployment step

For production environment, we recommend setting any deployment steps to manual (so deploys are triggered by user action and not automatically). To do that, you can add this condition in your `gitlab-ci.yml`:

```yml
deployment-job:
    script:
        - ...
    when: manual
```

### Runners configuration

Make sure that shared runners are disabled for the project, and only Leobit internal runners are used. If shared runners are not disabled, pipelines will fail for people with unverified Gitlab accounts (right now to verify the account you have to attach your credit card to account).

### Environments setup

If project uses some environment-specific CI/CD settings (for example, different database connection strings per environment), we recommend setting up "Environments". This will allow you to have multiple sets of CI/CD variables per environment. 

### Variables setup 

When setting up variables, check the following settings:
* All sensitive variables (API keys, credentials, etc.) are marked as "Masked".
* All variables that are only used in pipelines for protected branches (for example, pipelines that deploy from `master`) should be marked as "Protected"
* If variable is used in non-protected branches pipelines (for example, pull request pipelines) it shouldn't be marked as "Protected" (because it won't be visible there and pipeline will fail)
