# Conventions 
To streamline a bit our development process, let's implement some standard conventions.

## Branching conventions:
* Start your branch from latest develop if parent branch is not specified explicitly
* Branch name should consist of the following:
    * feature/ prefix
    * Ticket number and some meaningful name: feature/IRP-545-projects-table/
    * In case feature is huge and is going to have multiple PRs in same repository you can specify what exactly your branch does after the ticket name, for instance: feature/IPR-445-project-table/setup-external-integration
* For branches prefer lowercase (except for abbreviations) and use kebab case (feature/OT-545-project-table)

## Commits conventions:
* Don't commit something that doesn't build (even if it is immediately fixed in the following commit)
* Commit message:
    * Should be short (<80 chars)
    * Should start with capital letter
    * Don't put a dot (.) in the end
    * Shouldn't be a one word ("Fix", "Merge", "Temp", etc.)
    * Should be in present tense, for instance Add validation instead of Added validation or Adding validation. For easier understanding, just imagine that your commit goes after "If applied, this commit will ..."
    * Should be meaningful and really explain what this commit does ("Add additional fields to ProjectSchema", "Fix table margins in global style.css")

## PR/MR (Pull request/Merge request) conventions:
* Name your PR starting with ticket number (if there is one) + some meaningful description (IRP-608 | Filter shorcut buttons for datepickers)
* Include relevant information such as the link to issue or reference to a related pull request in the pull request description. For UI related PRs feel free to attach the screenshots of achieved result (this helps a lot).
* Keep the pull request small and focused, rather than bundling multiple unrelated changes into a single request. Usually short PRs are review quicker.
* Add inline comments to explain any complex or non-obvious code, especially if the logic or approach is not immediately clear from a first glance.
* Verify that the pull request passes all pipeline build, automated tests, sonar checks, etc. before asking for review.
* After approval you are responsible to resolve any merge conflicts merging the branch into the develop.

By following these practices, you can ensure that your pull requests are well-structured, easy to understand, and contribute positively to the development process. (edited) 