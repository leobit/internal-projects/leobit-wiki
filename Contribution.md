# Open source contribution guide

In order to successfully pass open-source contribution step of our promotion criterial, your contribution should satisfy few rules:
## Ruby
- The project or gem you like to contribute into should have more than 1k stars
- Changes should be functional (application ruby code; NOT locales, tests, comments, front-end in monolithic application etc)
- Pull request or several pull requests should totally contain at least 50 lines of code
- Pull request or several pull requests should be merged into gem repository OR in case of long merging process your pull requests at least should be attached to Opened Issue with reasonable discussion of added functionality)

## iOS
- The project you like to contribute should have more than 500 stars
- Changes should be functional (bug fixes, new features, improvements)
- Pull request or several pull requests should contain at least 30 lines of code
- Pull request or several pull requests should be merged into main repository

*\* Any rule could be discussed and skipped in case of previous approval by your Competence Manager* 

# How to contribute

Contributing to open source happens at all levels, across projects. You don’t need to overthink what exactly your first contribution will be, or how it will look.

Instead, start by thinking about the projects you already use, or want to use. The projects you’ll actively contribute to are the ones you find yourself coming back to.

## A checklist before you start to contribute

When you’ve found a project you’d like to contribute to, do a quick scan to make sure that the project is suitable for accepting contributions. Otherwise, your hard work may never get a response.

Here’s a handy checklist to evaluate whether a project is good for new contributors.

### Meets the definition of open source
- Does it have a license? Usually, there is a file called LICENSE in the root of the repository.
Project actively accepts contributions
- Look at the commit activity on the master branch. On GitHub, you can see this information on a repository’s homepage.
- When was the latest commit?
- How many contributors does the project have?
- How often do people commit?

### Look at the project’s issues.
- How many open issues are there?
- Do maintainers respond quickly to issues when they are opened?
- Is there active discussion on the issues?
- Are the issues recent?
- Are issues getting closed?

### Now do the same for the project’s pull requests.
- How many open pull requests are there?
- Do maintainers respond quickly to pull requests when they are opened?
- Is there active discussion on the pull requests?
- Are the pull requests recent?
- How recently were any pull requests merged?

### Project is welcoming
- A project that is friendly and welcoming signals that they will be receptive to new contributors.
- Do the maintainers respond helpfully to questions in issues?
- Are people friendly in the issues, discussion forum, and chat (for example, IRC or Slack)?
- Do pull requests get reviewed?
- Do maintainers thank people for their contributions?

## Follow the contribution rules

Please check if project have described some contribution guidelines, and follow them. If there are no specific rules just try to follow generic one

- Did you find a bug? Ensure the bug was not already reported by searching under issues.
- If you're unable to find an open issue addressing the problem, open a new one. Be sure to include a title and clear description, as much relevant information as possible, and a code sample or an executable test case demonstrating the expected behavior that is not occurring.
- If possible, use the relevant bug report templates to create the issue. Simply copy the content of the appropriate template, make the necessary changes to demonstrate the issue, and paste the content into the issue description:
- Did you write a patch that fixes a bug? Open a new GitHub pull request with the patch.
- Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.
- Before submitting, please read the guide to know more about coding conventions and benchmarks.
- Do not open an issue until you have collected positive feedback about the change. Issues are primarily intended for bug reports and fixes.

Thanks! 💚💚💚
